﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalanceAI.UtilTypes
{
    public interface IUtilizationVariance
    {
        int UpdateAppUtilization(double currentUtilization);
    }
}
