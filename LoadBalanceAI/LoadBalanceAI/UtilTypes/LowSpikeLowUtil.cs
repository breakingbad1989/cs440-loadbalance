﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalanceAI.UtilTypes
{
    public class LowSpikeLowUtil : IUtilizationVariance
    {
        // a random object reference to use as necessary
        private Random rand = new Random();

        // this is the random generated bound to keep the initial util within as this is a constant model
        private int UtilizationBound { get; set; }

        // this is the length that the utilization spike occurs in the units of updates
        private int SpikeInterval { get; set; }

        // length that utilization is normal
        private int NormalInterval { get; set; }

        private bool Spiking { get; set; }

        // this is the counter used to count time steps
        private int TimeCounter { get; set; }

        public int UpdateAppUtilization(double currentUtilization)
        {
            if (currentUtilization == 0)
            {
                TimeCounter = 0;
                // this will be the +- range from the initial util we will bounce around in.
                // HOWEVER, we will not allow utilzation to go to 25% for this experiment
                UtilizationBound = rand.Next(1, 10);

                // the random number of time (in units of updates) that specifies
                // how long the application will spike to High utilization
                SpikeInterval = rand.Next(5, 20);

                // random num of time (in units of updates) that specifies
                // how long the application will run normal
                NormalInterval = rand.Next(20, 100);

                // we choose an upper bound of 24 because the max utilization bound is 10 and we want the Low model to stay
                // under 25%
                int tempInt = rand.Next(0, 24);

                // since nextDouble uses the range 0.0-1.0 we need to move that decimal over with the tempInt acquired
                // this will be our initial util used by the app
                return tempInt;
            }
            else
            {   
                if (Spiking)
                {
                    int lowerBound = ((int)currentUtilization - UtilizationBound);
                    if (lowerBound < 50)
                        lowerBound = 50;

                    int upperBound = ((int)currentUtilization + UtilizationBound);
                    if (upperBound > 74)
                        upperBound = 74;

                    if (lowerBound > upperBound)
                    {
                        upperBound = lowerBound + 1;
                    }

                    if (TimeCounter == SpikeInterval)
                    {
                        TimeCounter = 0;
                        Spiking = false;
                    }
                    else
                        TimeCounter++;

                    return rand.Next(lowerBound, upperBound);
                }
                else
                {
                    int lowerBound = ((int)currentUtilization - UtilizationBound);
                    if (lowerBound < 0)
                        lowerBound = 0;

                    int upperBound = ((int)currentUtilization + UtilizationBound);
                    if (upperBound > 24)
                        upperBound = 24;

                    if (lowerBound > upperBound)
                    {
                        upperBound = lowerBound + 1;
                    }

                    if (TimeCounter == NormalInterval)
                    {
                        TimeCounter = 0;
                        Spiking = true;
                    }
                    else
                        TimeCounter++;


                    return rand.Next(lowerBound, upperBound);
                }

            }
        }
    }
}
