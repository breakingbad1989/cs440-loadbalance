﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LoadBalanceAI.UtilTypes;
using System.Threading;

namespace LoadBalanceAI
{
    // there are three main "levels" of applications in this model
    // -- Low, Medium, High --
    // however, at different phases the application may move through
    // There is an additional "Spike" level where the application demands
    // a High amount of CPU utilization but only for a brief period of time
    // which may not require being moved to another machine (if it is even able to)
    public enum AppUtilizationType
    {
        Low,
        Medium,
        High,
        LowMedLow,
        LowHighLow,
        MedHighMed,
        LowSpikeLow,
        MedSpikeMed
    }

    public class AppModel
    {
        public string NameOfApp { get; set; }

        public bool CanBeRestarted { get; set; }

        private string applicationUtilizationOutput = "";

        public AppUtilizationType UtilType;

        public IUtilizationVariance UtilizationModel;

        public int CurrentUtilUsedByApp { get; set; }

        public AppModel(string nameOfApp, bool canBeRestarted, AppUtilizationType utilType)
        {
            NameOfApp = nameOfApp;
            CanBeRestarted = canBeRestarted;
            CurrentUtilUsedByApp = 0;
            UtilType = utilType;

            switch (UtilType)
            {
                case AppUtilizationType.Low:
                    UtilizationModel = new LowUtil();
                    break;
                case AppUtilizationType.Medium:
                    UtilizationModel = new MediumUtil();
                    break;
                case AppUtilizationType.High:
                    UtilizationModel = new HighUtil();
                    break;
                case AppUtilizationType.LowMedLow:
                    UtilizationModel = new LowMedLowUtil();
                    break;
                case AppUtilizationType.LowHighLow:
                    UtilizationModel = new LowHighLowUtil();
                    break;
                case AppUtilizationType.MedHighMed:
                    UtilizationModel = new MedHighMedUtil();
                    break;
                case AppUtilizationType.LowSpikeLow:
                    UtilizationModel = new LowSpikeLowUtil();
                    break;
                case AppUtilizationType.MedSpikeMed:
                    UtilizationModel = new MedSpikeMedUtil();
                    break;
                default:
                    Console.WriteLine("UtilType was not set for the app called " + NameOfApp + " properly.");
                    break;
            }

            // this is how fast the app updates
            Timer t = new Timer(AppUtilizationUpdate, null, 0, 1000);
        }

        /// <summary>
        /// This will fire every so often to update this app's utilization value to emulate the
        /// real time behavior of an app's utilization.
        /// </summary>
        /// <param name="o"></param>
        private void AppUtilizationUpdate(object o)
        {
            CurrentUtilUsedByApp = UtilizationModel.UpdateAppUtilization(CurrentUtilUsedByApp);
        }

    }
}
