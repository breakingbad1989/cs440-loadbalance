﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoadBalanceAI
{
    public class MachineModel
    {
        public string NameOfMachine { get; set; }

        public int TotalCpuUtilization { get; set; }

        public List<AppModel> AppsRunningList;

        public MachineModel(string nameOfMachine, List<AppModel> initialAppsOnThisMachine)
        {
            NameOfMachine = nameOfMachine;
            AppsRunningList = initialAppsOnThisMachine;
            TotalCpuUtilization = 0;
        }

        public void UpdateTotalCpuUtilization()
        {
            TotalCpuUtilization = 0;
            foreach(var app in AppsRunningList)
            {
                TotalCpuUtilization += app.CurrentUtilUsedByApp;
            }
            if (TotalCpuUtilization > 100)
                TotalCpuUtilization = 100;
        }

        public void PrintAppsRunning()
        {
            Console.WriteLine("List of apps running on after update " + NameOfMachine + ":");
            foreach (var app in AppsRunningList)
            {
                Console.WriteLine(app.NameOfApp);
            }
            Console.Write("\n");
        }

        public void PrintTotalUtil()
        {
            Console.WriteLine("Total util used by this machine: " + TotalCpuUtilization);
        }
    }
}
