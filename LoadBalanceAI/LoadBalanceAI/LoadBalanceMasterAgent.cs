﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;


using MoreLinq;

namespace LoadBalanceAI
{
    public class LoadBalanceMasterAgent
    {
        // represents the max utilization allowed per machine to keep from
        // completely maxing out processor, only one High util process should
        // ever be on one processor
        private int maxUtilizationPerMachine = 75;

        // this will be the inner class used in a dict to keep track of an
        // apps running stats
        private class AppStats
        {
            public string appName;
            public int sampleTotal = 0;
            public double currentMean = 0.0;
            public double sumOfSquaresOfDiff = 0.0;
            public double currentVariance = 0.0;
            public double currentSD = 0.0;
        }

        Dictionary<string, AppStats> AppStatsDict = new Dictionary<string, AppStats>();

        public List<MachineModel> MachineList { get; set; }

        public LoadBalanceMasterAgent()
        {
            MachineList = new List<MachineModel>();
        }

        // this event is emulating the message heartbeat of every x seconds for this Master
        // agent to run another assessment across the LAN, and after figuring out what it wants to do
        // will change move apps to the appropriate machine
        public void UpdateLoadBalance()
        {

            //update total utilization of the machine the "slave" is running on to report back to master agent
            foreach (var machine in MachineList)
            {
                // first need to update cpu util for each machine
                machine.UpdateTotalCpuUtilization();
                Console.WriteLine("Total util used by " + machine.NameOfMachine + " before update: " + machine.TotalCpuUtilization);

                // need to get statistical updates from each app on this machine
                foreach (var app in machine.AppsRunningList)
                {
                    // if a app stat entry does not yet exist for this app in the agent dictionary, we need to create one
                    if (!AppStatsDict.ContainsKey(app.NameOfApp))
                    {
                        AppStats newAppStat = new AppStats();
                        newAppStat.appName = app.NameOfApp;
                        AppStatsDict.Add(app.NameOfApp, newAppStat);
                    }


                    // GET THE UPDATED STATS OF THIS APP
                    AppStats statReference = AppStatsDict[app.NameOfApp];
                    // update the current mean for this app
                    statReference.currentMean = CalculateRunningMean(statReference.currentMean, statReference.sampleTotal, app.CurrentUtilUsedByApp);


                    // update the current variance next
                    var delta = app.CurrentUtilUsedByApp - statReference.currentMean;
                    statReference.sumOfSquaresOfDiff += delta * delta;
                    statReference.currentVariance = statReference.sumOfSquaresOfDiff / (statReference.sampleTotal - 1);

                    // finally get the standard deviation from the variance
                    statReference.currentSD = Math.Sqrt(statReference.currentVariance); 
                }
            }
            
            // Now that all the apps in each machine are updated as well as total cpu utilization for each machine
            // we want to find out which machine(s) has the lowest CPU utilization and put more weight on them and
            // take off pressure on machines running harder
            List<MachineModel> machinesToLoadBalance = new List<MachineModel>();
            foreach (var machine in MachineList)
            {
                // collect all apps are over the max util allowance
                if (machine.TotalCpuUtilization > maxUtilizationPerMachine)
                    machinesToLoadBalance.Add(machine);
            }

            // next we want to move apps from those to the best candidate
            List<AppModel> appsWithLowestMean = new List<AppModel>();
            
            foreach (var machine in machinesToLoadBalance)
            {
                var lowestMeanApp = 1000;
                AppModel appWithLowestMeanOnThisMachine = null;
                foreach (var appToMove in machine.AppsRunningList)
                {

                    var appMean = AppStatsDict[appToMove.NameOfApp].currentMean;
                    var appSD = AppStatsDict[appToMove.NameOfApp].currentSD;
                    
                    if (appMean < lowestMeanApp)
                        appWithLowestMeanOnThisMachine = appToMove;
                }
                // remove the app from this machine because we're about to start it on another
                // with lowest utilization
                machine.AppsRunningList.Remove(appWithLowestMeanOnThisMachine);
                appsWithLowestMean.Add(appWithLowestMeanOnThisMachine);
            }

            // now we want to move it to the machine with the lowest utilization
            foreach (var app in appsWithLowestMean)
            {
                // this line searches for the machine with the lowest total utilization and starts an app
                // selected from an overloaded machine to this machine
                MachineList.MinBy(x => x.TotalCpuUtilization).AppsRunningList.Add(app);
            }
            
            foreach(var machine in MachineList)
            {
                machine.UpdateTotalCpuUtilization();
                machine.PrintAppsRunning();
                Console.WriteLine("Total util used by " + machine.NameOfMachine + " AFTER update: " + machine.TotalCpuUtilization);
            }
       }

        public double CalculateRunningMean(double currentMean, int currentSetSize, double nextVal)
        {
            var weightedSum = currentMean * currentSetSize;
            weightedSum = weightedSum + nextVal;
            var newSetSize = currentSetSize + 1;
            return weightedSum / newSetSize;
        }
    }
}