﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace LoadBalanceAI
{
    class Program
    {
        // this is the update rate of the master agent
        static int masterAgentUpdateRate = 2000;

        static void Main(string[] args)
        {
            // first create all the apps we want to have in the simulation
            AppModel app1 = new AppModel("App 1", true, AppUtilizationType.High);
            AppModel app2 = new AppModel("App 2", true, AppUtilizationType.Medium);
            AppModel app3 = new AppModel("app 3", true, AppUtilizationType.Low);
            AppModel app4 = new AppModel("app 4", false, AppUtilizationType.LowSpikeLow);
            AppModel app5 = new AppModel("app 5", true, AppUtilizationType.LowSpikeLow);
            AppModel app6 = new AppModel("app 6", true, AppUtilizationType.Low);
            AppModel app7 = new AppModel("app 7", true, AppUtilizationType.MedSpikeMed);
            AppModel app8 = new AppModel("app 8", true, AppUtilizationType.High);
            AppModel app9 = new AppModel("app 9", true, AppUtilizationType.High);

            // next create lists to collect each machines' initial apps
            List<AppModel> machine1InitApps = new List<AppModel>();
            machine1InitApps.Add(app1);
            machine1InitApps.Add(app2);
            machine1InitApps.Add(app3);
            List<AppModel> machine2InitApps = new List<AppModel>();
            machine2InitApps.Add(app4);
            machine2InitApps.Add(app5);
            List<AppModel> machine3InitApps = new List<AppModel>();
            machine3InitApps.Add(app6);
            machine3InitApps.Add(app7);
            List<AppModel> machine4InitApps = new List<AppModel>();
            machine4InitApps.Add(app8);
            machine4InitApps.Add(app9);

            // creating the master agent
            LoadBalanceMasterAgent machineMasterAgent = new LoadBalanceMasterAgent();

            // create the machines needed in the simulation, passing in the list of initial apps
            // it will run
            MachineModel machine1 = new MachineModel("machine 1", machine1InitApps);
            MachineModel machine2 = new MachineModel("machine 2", machine2InitApps);
            MachineModel machine3 = new MachineModel("machine 3", machine3InitApps);
            MachineModel machine4 = new MachineModel("machine 4", machine4InitApps);

            // add the machines to the agent to observe
            machineMasterAgent.MachineList.Add(machine1);
            machineMasterAgent.MachineList.Add(machine2);
            machineMasterAgent.MachineList.Add(machine3);
            machineMasterAgent.MachineList.Add(machine4);

            while (true)
            {
                machineMasterAgent.UpdateLoadBalance();
                Console.WriteLine("UPDATE COMPLETE\n\n\n");
                Thread.Sleep(masterAgentUpdateRate);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }
    }
}
